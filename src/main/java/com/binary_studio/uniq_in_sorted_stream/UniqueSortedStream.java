package com.binary_studio.uniq_in_sorted_stream;

import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		long[] buff = new long[1];
		return stream.filter(row -> {
			boolean exists = buff[0] == row.getPrimaryId();
			buff[0] = row.getPrimaryId();
			return !exists;
		});
	}

}
