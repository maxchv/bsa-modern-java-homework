package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		return rootDepartment.subDepartments.stream().mapToInt(d -> calculateMaxDepth(d, 1)).max().orElse(1);
	}

	private static int calculateMaxDepth(Department d, int i) {
		if (d == null) {
			return i;
		}
		return d.subDepartments.stream().mapToInt(d1 -> calculateMaxDepth(d1, i + 1)).max().orElse(i + 1);
	}

}
