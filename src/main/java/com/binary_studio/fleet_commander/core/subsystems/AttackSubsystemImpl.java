package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger powergridRequirments;

	private final PositiveInteger capacitorConsumption;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger optimalSize;

	private final PositiveInteger baseDamage;

	private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powergridRequirments = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	/**
	 * Атакующая подсистема работает следующим образом: в её метод attack передаётся цель,
	 * после чего производится расчет нанесенного урона по следующей формуле:
	 *
	 * <pre>
	 * sizeReductionModifier =  when targetSize >= optimalSize -> 1
	 *                          else targetSize / optimalSize
	 * speedReductionModifier = when targetSpeed <= optimalSpeed -> 1
	 *                          else optimalSpeed / (2 * targetSpeed)
	 * damage = baseDamage * min(sizeReductionModifier, speedReductionModifier)
	 * </pre>
	 *
	 * <p>
	 * Полученный урон округляется до целого числа с округлением в большую сторону(т.е.
	 * при уроне больше 0, атака не может нанести меньше 1 единицы урона).
	 *
	 * <p>
	 * Для атаки требуется определённое количество заряда аккумулятора. При попытке
	 * атаковать с недостаточным количеством заряда аккумулятора необходимо вернуть пустой
	 * Optional.
	 * @param target - цель атаки
	 * @return полученный урон.
	 */
	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
				: ((double) target.getSize().value()) / this.optimalSize.value();
		double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: this.optimalSpeed.value() / (2. * target.getCurrentSpeed().value());
		return PositiveInteger
				.of((int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
