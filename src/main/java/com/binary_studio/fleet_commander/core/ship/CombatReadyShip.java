package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name; // Название корабля

	private final PositiveInteger shieldHP; // Состояние щита, `EHP`

	private PositiveInteger currentShieldHP; // Состояние щита, `EHP`

	private final PositiveInteger hullHP; // Состояние корпуса, `EHP`

	private PositiveInteger currentHullHP; // Состояние корпуса, `EHP`

	private final PositiveInteger powergridOutput; // Мощность реактора, `MW`

	private final PositiveInteger capacitorAmount; // Заряд аккумулятора, `GA/H`

	private PositiveInteger currentCapacitor; // Текущий заряд аккумулятора, `GA/H`

	private final PositiveInteger capacitorRechargeRate; // Восстановление заряда

	// аккумулятора в ход

	private final PositiveInteger speed; // Скорость `m/s`

	private final PositiveInteger size; // Размер, `m`

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.currentShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.currentHullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.currentCapacitor = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public void setAttackSubsystem(AttackSubsystem attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public void setDefenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
	}

	public <T extends Subsystem> T fitSubsystem(T subsystem, Subsystem other) throws InsufficientPowergridException {
		if (subsystem == null) {
			return null;
		}
		if (other == null) {
			if (subsystem.getPowerGridConsumption().value() <= this.powergridOutput.value()) {
				return subsystem;
			}
			else {
				throw new InsufficientPowergridException(
						subsystem.getPowerGridConsumption().value() - this.powergridOutput.value());
			}
		}
		else {
			int power = other.getPowerGridConsumption().value() + subsystem.getPowerGridConsumption().value();
			if (this.powergridOutput.value() >= power) {
				return subsystem;
			}
			else {
				throw new InsufficientPowergridException(power - subsystem.getPowerGridConsumption().value());
			}
		}
	}

	/**
	 * В конце хода корабль восстанавливает заряд аккумулятора. Заряд аккумулятора не
	 * может быть заряжен больше начального значения.
	 */
	@Override
	public void endTurn() {
		int val = this.currentCapacitor.value() + this.capacitorRechargeRate.value();
		this.currentCapacitor = (val <= this.capacitorAmount.value()) ? PositiveInteger.of(val) : this.capacitorAmount;
	}

	/**
	 * Корабль оповещается о начале и конце хода.
	 */
	@Override
	public void startTurn() {
		if (this.currentHullHP.value() < this.hullHP.value() || this.currentShieldHP.value() < this.shieldHP.value()) {
			int val = this.currentCapacitor.value() - this.defenciveSubsystem.getCapacitorConsumption().value();
			if (val > 0) {
				this.currentCapacitor = PositiveInteger.of(val);
			}
		}
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	/**
	 * При попытке атаковать с недостаточным количеством заряда аккумулятора необходимо
	 * вернуть пустой Optional.
	 * @param target - объект атаки
	 * @return результат атаки
	 */
	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.currentCapacitor.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		Integer requireCapacitor = this.attackSubsystem.getCapacitorConsumption().value();
		Integer actualCapacitor = this.currentCapacitor.value();
		this.currentCapacitor = PositiveInteger.of(actualCapacitor - requireCapacitor);
		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	/**
	 * Для активации защитной системы необходимо определённое количество заряда. При
	 * получении урона у корабля уменьшается количество щита и корпуса. Сначала урон
	 * получает щит. Если здоровье щита упало до 0, то остаток урона получает корпус.
	 * @param attack - атака на корабль
	 * @return результат атаки
	 */
	@Override
	public AttackResult applyAttack(AttackAction attack) {
		if (this.currentCapacitor.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			attack = this.defenciveSubsystem.reduceDamage(attack);
		}
		Integer damage = attack.damage.value();
		Integer shieldHP = this.currentShieldHP.value();
		Integer hullHP = this.currentHullHP.value();
		if (shieldHP >= damage) {
			shieldHP -= damage;
			this.currentShieldHP = PositiveInteger.of(shieldHP);
		}
		else if (shieldHP > 0) {
			int d = damage - shieldHP;
			this.currentShieldHP = PositiveInteger.of(0);
			hullHP -= d;
			this.currentHullHP = PositiveInteger.of(hullHP);
		}
		else {
			hullHP -= damage;
			if (hullHP <= 0) {
				return new AttackResult.Destroyed();
			}
			this.currentHullHP = PositiveInteger.of(hullHP);
		}
		return new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
	}

	/**
	 * Активация восстанавливает EHP корпуса и щита. При регенерации щит и корпус
	 * восстанавливаются, используя регенерацию защитной подсистемы. Здоровье щитов и
	 * корпуса не может превышать изначальное значение.
	 * @return результат регенерации
	 */
	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.currentCapacitor.value() < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		if (this.currentShieldHP.equals(this.shieldHP) && this.currentHullHP.equals(this.hullHP)) {
			return Optional.of(new RegenerateAction(PositiveInteger.of(0), PositiveInteger.of(0)));
		}
		RegenerateAction regenerate = this.defenciveSubsystem.regenerate();
		if (this.hullHP.value().equals(this.currentHullHP.value())) {
			regenerate = new RegenerateAction(regenerate.shieldHPRegenerated, PositiveInteger.of(0));
		}
		if (regenerate.shieldHPRegenerated.value() > 0) {
			this.currentCapacitor = PositiveInteger
					.of(this.currentCapacitor.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
		}
		return Optional.of(regenerate);
	}

}
