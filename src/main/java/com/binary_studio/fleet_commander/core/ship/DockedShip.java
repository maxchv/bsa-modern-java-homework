package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final CombatReadyShip dockedCombatShip;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {

		this.dockedCombatShip = new CombatReadyShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount,
				capacitorRechargeRate, speed, size);
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	/**
	 * Атакующая система требует определённое количество PG. Потребление PG суммируется,
	 * т.е. для того, чтобы установить две подсистемы с требованием к PG 50 MW и 25 MW
	 * потребуется корабль с 25 + 50 => 75 MW.
	 * <p>
	 * При недостатке мощности реактора необходимо выбросить
	 * InsufficientPowergridException.
	 * @param subsystem - устанавливаема подсистема атаки, если null - снимает ее
	 * @throws InsufficientPowergridException - при недостатке мощности реактора
	 */
	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		var attackSubsystem = this.dockedCombatShip.fitSubsystem(subsystem,
				this.dockedCombatShip.getDefenciveSubsystem());
		this.dockedCombatShip.setAttackSubsystem(attackSubsystem);
	}

	/**
	 * Защитная система требует определённое количество PG. Потребление PG суммируется,
	 * т.е. для того, чтобы установить две подсистемы с требованием к PG 50 MW и 25 MW
	 * потребуется корабль с 25 + 50 => 75 MW.
	 * <p>
	 * При недостатке мощности реактора необходимо выбросить
	 * InsufficientPowergridException.
	 * @param subsystem - устанавливаема подсистема защиты, если null - снимает ее
	 * @throws InsufficientPowergridException - при недостатке мощности реактора
	 */
	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		var defenciveSubsystem = this.dockedCombatShip.fitSubsystem(subsystem,
				this.dockedCombatShip.getAttackSubsystem());
		this.dockedCombatShip.setDefenciveSubsystem(defenciveSubsystem);
	}

	/**
	 * Изначально корабль находится в доке, в этом режиме к кораблю нужно подключать
	 * модули. Корабль не может выйти из дока, пока на него не установлена защитная и
	 * атакующая подсистемы.
	 * @return Готовый к бою корабль с установленными защитной и атакующей подсистемами
	 * @throws NotAllSubsystemsFitted при отсутствии установленных атакующей и/или
	 * защитной подсистем
	 */
	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.dockedCombatShip.getAttackSubsystem() == null
				&& this.dockedCombatShip.getDefenciveSubsystem() == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.dockedCombatShip.getAttackSubsystem() == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.dockedCombatShip.getDefenciveSubsystem() == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return this.dockedCombatShip;
	}

}
